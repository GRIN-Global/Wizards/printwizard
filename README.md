## How to install the Print Wizard for Curator Tool

1. Download and copy the file "PrintWizard.dll" to the folder "C:\Program Files\GRIN-Global\GRIN-Global Curator Tool\Wizards"
2. Unblock the DLL file: right click the DLL file, select properties and check "Unblock"
3. Run Curator Tool

## Training Videos on YouTube
Go to [https://www.youtube.com/watch?v=P7p8WSJRRt4&list=PLrY0roojbpPUp3kUAwckkBDKXJMxkBSu_](https://www.youtube.com/watch?v=P7p8WSJRRt4&list=PLrY0roojbpPUp3kUAwckkBDKXJMxkBSu_)

## Source code
Go to [https://gitlab.com/CIP-Development/grin-global_client_international](https://gitlab.com/CIP-Development/grin-global_client_international)

